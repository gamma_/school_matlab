clear;clc;

%% Input VariabLes
g = 9.81; % m/s^2
L1 = 2; % m
L2 = 1; % m
L = L1 + L2;
a1 = pi/8; % rad
a2 = 0; % rad
w1 = 0; % rad/s
w2 = 0; % rad/s
m1 = 10; % kg
m2 = 5; % kg
tf = 10; % s

% Starting point calc
x10 = L1*sin(a1);
x20 = x10 + L2*sin(a2);
y10 = -L1*cos(a1);
y20 = y10 - L2*cos(a2);

%% Loop
% Initialize Time
ti = 0;
% Step Size
h = 50e-6;
% Interval for plotting
tp = 0.03;
% Setup Matricies
w = [0,0];
Loc = zeros(3,2);
wp = zeros(2,tf/h);
p2 = zeros(2,tf/h);
% Initial Conditions
Loc(2,1) = x10;
Loc(2,2) = y10;
Loc(3,1) = x20;
Loc(3,2) = y20;
peo = m1*g*(Loc(2,2)+L) + m2*g*(Loc(3,2)+L);
% Setup plot
figure(1); clf(1);
% Iterate
b = L/6;
while ti <= tf
    % RK 4 for angular velocity
    dw1 = @(w1, w2) (-g*(2*m1 + m2)*sin(a1) - g*m2*sin(a1-2*a2) - 2*m2*sin(a1-a2)*(L2*(w2)^2 + L1*(w1)^2*cos(a1 - a2)))/(L1*(2*m1 + m2 - m2*cos(2*a1 - 2*a2)));
    dw2 = @(w1, w2) (2*sin(a1-a2)*(w1^2*L1*(m1 + m2) + g*(m1 + m2)*cos(a1) + w2^2*L2*m2*cos(a1 - a2)))/(L2*(2*m1 + m2 - m2*cos(2*a1 - 2*a2)));
    dw = {dw1, dw2};
    wi = [w1, w2];
    for i = 1:2
        dwi = dw{i};
        k1 = dwi(wi(1), wi(2)); % k1 = dydx(xo, yo)
        k2 = dwi(wi(1)+k1*h/2, wi(2)+k1*h/2); % k2 = dydx(xo+h, yo+k1)
        k3 = dwi(wi(1)+k2*h/2, wi(2)+k2*h/2); % k3= dydx(xo+h/2, yo+k2/2
        k4 = dwi(wi(1)+k3*h, wi(2)+k3*h); % k4 = dydx(xo+h, yo+k3*h)
        w(i) = wi(i) + h*(k1 + 2*k2 + 2*k3 + k4)/6;
    end
    % Calculate Angles
    a1 = a1 + h*w(1);
    a2 = a2 + h*w(2);
    Loc(2,1) = L1*sin(a1); Loc(2,2) = -L1*cos(a1); 
    Loc(3,1) = Loc(2,1) + L2*sin(a2); Loc(3,2) = Loc(2,2) - L2*cos(a2);
    p2(1,round(ti/h)+1) = Loc(3,1);p2(2,round(ti/h)+1) = Loc(3,2); 
    % Store W's
    wp(1, round(ti/h)+1) = w1; wp(2, round(ti/h)+1) = w2; 
    w1 = w(1);w2 = w(2);
    % Increment time and determine whether to plot
    if rem(ti/h,tp/(h)) < 1 || ti+h >= tf
%         pe = m1*g*(Loc(2,2)+L) + m2*g*(Loc(3,2)+L);
%         pep = 100*pe/peo;
%         ke = m1*(w1*L1)^2/2 + m2*(w2*L2)^2/2;
%         kep = 100*ke/peo;
%         te = pep + kep;
        plot(Loc(:,1), Loc(:, 2), 'k')
        title(['Time: ' num2str(round(ti,2)) 's'])
        xlim([-L-b L+b]); ylim([-L-b L+b]);
%         text(-L, L,['PE: ' num2str(pep)])
%         text(-L, L-L/8,['KE: ' num2str(kep)])
%         text(-L, L-2*L/8,['E: ' num2str(te)])
        hold on
        plot(p2(1,1:round(ti/h)+1),p2(2,1:round(ti/h)+1),'r')
        r = 20;
        plot(Loc(2,1),Loc(2,2), 'bo', 'MarkerSize', r, 'MarkerFaceColor', 'b')
        plot(Loc(3,1),Loc(3,2), 'bo', 'MarkerSize', r*m2/m1, 'MarkerFaceColor', 'r')
        hold off
        drawnow
    end
    ti = ti + h;
end
% Plot Angular Velocities
figure(2);clf(2);
plot(linspace(0,tf,tf/h), wp(1,1:tf/h))
title('Angular Velocities vs. Time');xlabel('Time(s)');ylabel('Angular Velocity(radians/s)')
hold on
plot(linspace(0,tf,tf/h), wp(2,1:tf/h))
hold off
legend('W of 1st Mass','W of 2nd Mass','Location', 'southeast')