clear; clc;format long
% Defining Initial Variables
a0 = 1;
b0 = 1/sqrt(2);
t0 = 1/4;
p0 = 1;
% Loop
for i = 0:2
    a1 = (a0+b0)/2;
    b1 = sqrt(a0*b0);
    t1 = t0 - p0*(a0-a1)^2;
    p1 = p0*2;
    a0 = a1;
    b0 = b1;
    t0 = t1;
    p0 = p1;
end
% Final Estimation
est = (a1+b1)^2/(4*t1);
disp(est)
% This one required much less iterations to find PI to 12 Digits