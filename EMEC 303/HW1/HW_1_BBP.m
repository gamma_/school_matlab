clear; clc;format long
% Function Definition
f = @(x) 1/(16^x)*(4/(8*x+1)-2/(8*x+4)-1/(8*x+5)-1/(8*x+6));
% Loop for Sum
est = 0;
for i = 0:7
    est = est + f(i);
end
disp(est)
% This one required more iterations to find PI to 12 digits