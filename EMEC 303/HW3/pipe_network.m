% Fixed point iteration of pipe system
clear; clc

f=0.005;  % Friction factor
rho=1.23; % Density (kg/m^3)
D=0.01;   % Pipe diameter (m)
% Pressure drop through pipe with length L and flow rate Q
dP  =@(L,Q) 16/pi^2*f*L*rho/(2*D^5)*Q.^2;
dPdQ=@(L,Q) 16/pi^2*f*L*rho/(D^5)*Q;  %d(deltaP)/dQ

% Input parameters
Q1=1; % m^3/s
L2=2; % m
L3=1; % m
L4=2; % m
L5=4; % m
L6=1; % m
tol=1e-5;  % Convergence tolerance

% Inital pressure drop and flow rate guesses
Q2=0.6; % m^3/s - assumes equal split at each intersection
Q3=Q1-Q2;
Q5=0.05;
Q4=Q3-Q5;
Q6=Q3;

figure(1); clf(1)

% Iterate
alpha=.08;
N=2000;
Qs=zeros(N,5);
for n=1:N    
    % Store old values
    Q2o=Q2; Q3o=Q3; Q4o=Q4; Q5o=Q5; Q6o=Q6;
    
    % Update
    Q2=Q2o+alpha*(Q1 -Q2o-Q3o);
    Q3=Q3o - alpha*(Q3o - Q4o - Q5o);
    Q4=Q4o-alpha*(dP(L2,Q2o)-dP(L3,Q3o)-dP(L4,Q4o)-dP(L6,Q6o)) ...
        /(-dPdQ(L4,Q4o));
    Q5=Q5o - alpha*(dP(L5, Q5o) - dP(L4, Q4o))/(dPdQ(L5,Q5o));
    Q6=Q6o - alpha*(Q6o - Q3o);
        
    % Plot flow rates versus iteration
    Qs(n,:)=[Q2,Q3,Q4,Q5,Q6];
    if mod(n,10)==1
        plot(1:n,Qs(1:n,:))
        legend('Q2','Q3','Q4','Q5','Q6')
        drawnow
    end
    
    % Check if converged
    if max([abs(Q2-Q2o),abs(Q3-Q3o),abs(Q4-Q4o),abs(Q5-Q5o)])<tol
        break % Stop for loop
    end
end

% Display output
    fprintf('Iter=%2i  Q1=%5.3f Q2=%5.3f  Q3=%5.3f  Q4=%5.3f  Q5=%5.3f Q6=%5.3f \n', ...
        n,Q1,Q2,Q3,Q4,Q5,Q6)
% Rounding for tolerance
%Define dPi
dP2 = round(dP(L2, Q2),-5);
dP3 = round(dP(L3, Q3),-5);
dP4 = round(dP(L4, Q4),-5);
dP5 = round(dP(L5, Q5),-5);
dP6 = round(dP(L6, Q6),-5);
% Define Qi
Q1 = round(Q1, 3);
Q2 = round(Q2, 3);
Q3 = round(Q3, 3);
Q4 = round(Q4, 3);
Q5 = round(Q5, 3);
Q6 = round(Q6, 3);
% Check EQN.1
if Q1 == (Q2 + Q3)
    disp('EQN.1 is True')
else
    disp('EQN.1 is False')
end
% Check EQN.2
if Q3 == (Q4 + Q5)
    disp('EQN.2 is True')
else
    disp('EQN.2 is False')
end
% Check EQN.3
if Q3 == Q6
    disp('EQN.3 is True')
else
    disp('EQN.3 is False')
end
% Check EQN.4
if dP2 == (dP3 + dP4 + dP6)
    disp('EQN.4 is True')
else
    disp('EQN.4 is False')
end
% Check EQN.5
if dP4 == dP5
    disp('EQN.5 is True')
else
    disp('EQN.5 is False')
end
