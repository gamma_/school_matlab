clear;clc;

% Initial Constants
h = 10;
xo = 0;
yo = 0;
n = 25;
cin = 2;
q = 0.1;
v = 5;

% Setup Functions
syms x y
dydx = q/v*(cin-y); % Given Differential Equation
d2ydx2 = diff(dydx, y);
y = cin*(1-exp(-q/v*x)); % Particular Solution to Given Diff. Eq.
dydx = matlabFunction(dydx);
d2ydx2 = matlabFunction(d2ydx2);
y = matlabFunction(y);

% Create Figure & Preallocate
figure(1); clf(1)
pos = zeros(n,2);
% Iterate
for i = 1:n
    pos(i,1) = xo;
    pos(i,2) = yo;
    % Original Euler Method
    y1 = yo + h*(dydx(yo));
    % Average slope for Heun's Method
    ave = (dydx(y1)+dydx(yo))/2;
    % Improved Guesstimate using Heun
    y1 = yo + h*ave;
    plot(pos(1:i,1),pos(1:i,2))
    drawnow
    xo = xo + h;
    yo = y1;
end
% Plot Analytical Solution
hold on
xl = linspace(0, xo, 1000);
plot(xl,y(xl))
% Add Legend
legend('Euler', 'Analytical')