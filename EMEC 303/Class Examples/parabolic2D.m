% 1D Parabolic PDE
clear; clc

% Define constants
k=0.01;         % Heat transfer coefficient
L=1;            % Length of the bar
Nx=30;          % Number of points along bar
dt=0.005;       % Timestep
Tfinal=100;     % Total simulation time
Nt=Tfinal/dt;   % Number of timesteps

% Create grid along bar
x=linspace(0,L,Nx);
y = x;
dx=x(2)-x(1);
dy = dx;

% Initial condition/Boundary conditions
T=zeros(Nx,Nx); % IC T=0 @ t=0
T(1,:)=15;      % Top  BC T=10 @ x=0
T(:,end)= 20;      % Right BC T=20 @ x=L
T(:,1) = 10;
T(end,:) = 10;


% Initialize time
t=0;

% Loop over time
for n=1:Nt-1
    % Update time
    t=t+dt;
    % Loop over interior grid points
    for j = 2:Nx-1
        for i=2:Nx-1
            %n = i + (j-1)*Nx;
            % Store old Temp
            To=T;
            % Update Temp T^(n+1)
            T(i,j)=To(i,j)+dt*k*( ...
                (To(i-1,j)-2*To(i,j)+To(i+1,j))/dx^2+...
                (To(i,j-1)-2*To(i,j)+To(i,j+1))/dy^2);
        end
    end
    % Plot temperature at this time
    if rem(n,100)==0
        figure(1); clf(1)
        surf(x,y,T)
        title(['Time = ',num2str(t)])
        drawnow
    end
end
    



