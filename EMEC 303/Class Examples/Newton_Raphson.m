% Stuff things
clear; clc;

% Define Function
% Yay symbolic math
syms x
f = x^3-5*x^2+7*x-3;
fp = diff(f);
fpp = diff(fp);

f = matlabFunction(f);
fp = matlabFunction(fp);
fpp = matlabFunction(fpp);

% Guess
x = 0;
x_mod = 0;

% Iteration
iter = 0;
iter_mod = iter;

% Tolerance
tol = 1e-9;

% Iterate
while abs(f(x)) > tol
    iter = iter + 1;
    % Compute new estimate
    x = x - f(x)/fp(x);
end
while abs(f(x_mod)) > tol
    iter_mod = iter_mod + 1;
    % Compute new estimate
    x_mod = x_mod - (f(x_mod)*fp(x_mod))/(fp(x_mod)^2-f(x_mod)*fpp(x_mod));
end
% Output
fprintf('Original NR:\nRoot = %5.5f \nIterations = %d\n', x, iter)
fprintf('Modified NR:\nRoot = %5.5f \nIterations = %d\n', x_mod, iter_mod)