% Solve shallow water equations
clear; clc

% Parameters
Lx = 5;          % Domain size
Ly = 5;
b  = 1;          % Diffusion coefficient
g  = 9.81;        % Gravitational acceleration
H  = 0.5;          % Mean water height
Nx = 32;         % Number of grid points
Ny = 32;
dt = 0.00005;     % Timestep size
Tfinal = 3; % End of simulation time

% Create gird
x=linspace(-Lx/2,Lx/2,Nx);
y=linspace(-Ly/2,Ly/2,Ny);
dx=x(2)-x(1);
dy=y(2)-y(1);

% Initial condition for u,v,h
t=0;
u=zeros(Nx,Ny);
v=zeros(Nx,Ny);
xo=0.25;
yo=0.25;
sigma=min(Lx,Ly)/10;
A=1;
h=zeros(Nx,Ny);
for i=1:Nx
    for j=1:Ny
        h(i,j)=A*exp(-((x(i)-xo)^2+(y(j)-yo)^2)/(2*sigma^2));
    end
end

% Loop over time
for n=1:Tfinal/dt
    % Update time
    t=t+dt;
    
    % Store old solution
    ho=h;
    uo=u;
    vo=v;
    % Update velocity and heights
    for i=2:Nx-1
        for j=2:Ny-1
            % Derivatives
            dhdx=(ho(i+1,j  )-ho(i-1,j  ))/(2*dx);
            dhdy=(ho(i  ,j+1)-ho(i  ,j-1))/(2*dy);
            dudx=(uo(i+1,j)-uo(i-1,j))/(2*dx);
            dvdy=(vo(i,j+1)-vo(i,j-1))/(2*dy);
            % Update
            u(i,j)=uo(i,j)+dt*(-g*dhdx-b*uo(i,j));
            v(i,j)=vo(i,j)+dt*(-g*dhdy-b*vo(i,j));
            h(i,j)=ho(i,j)+dt*-H*(dudx+dvdy);
        end
    end
    
    % Apply BC's
    u( 1, :)=-u(   2,   :);
    u(Nx, :)=-u(Nx-1,   :);
    u( :, 1)=+u(   :,   2);
    u( :,Ny)=+u(   :,Ny-1);
    
    v( 1, :)=v(2, :);
    v(Nx, :)=v(Nx-1, :);
    v( :, 1)=-v(:, 2);
    v( :,Ny)=-v(:, Ny-1);
    
    h( 1, :)=+h(   2,   :);
    h(Nx, :)=+h(Nx-1,   :);
    h( :, 1)=h(:, 2);
    h( :,Ny)=h(:,Ny-1);
    
    % Plot interior points
    if (mod(n,500)==1)
        surf(x,y,H+h')
        axis([-Lx/2,Lx/2,-Ly/2,Ly/2,0,1.5])
        shading interp
        drawnow
    end
end


