clear; clc;

%% Preallocate
N = 100;
T = zeros(N,N);

%% Initial Guess
Ti = 15;
T = T + Ti;

%% Set Boundary Conditions
Tt = 10;
Tb = 10;
Tr = 20;
Tl = 10;
T(:,1) = Tl;
T(:,end) = Tr;
T(1,:) = Tt;
T(end,:) = Tb;

%% Iterate
for n = 1:1000
    for j = 2:N-1
        for i = 2:N-1
            T(i,j) = (T(i-1,j)+T(i+1,j)+T(i,j-1)+T(i,j+1))/4;
        end
    end
end

%% Plot
figure(1);clf(1);
x = linspace(0,1,N);
y = linspace(0,1,N);
contour(x,y,T)
