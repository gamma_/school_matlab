clear;clc

% In
m = 5;
k = 0.5;
c = [0, sqrt(4*m*k)-1, sqrt(4*m*k)+1, sqrt(4*m*k)];
l = 1;

% Define System of Eqn.
f = @(x,y) [ y(2);m/(E*I)];
        
% Set initial condition
y0 = [0,0];
tspan = [0 l];

% Solve
[x,y] = ode45(f,tspan,y0);

% Plot
figure(1); clf(1)
plot(x, y(:,1))
xlabel('x (m)'); ylabel('Displacement');
