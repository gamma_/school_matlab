% Secant Method
clear; clc;

% Define Function
f = @(x) sin(x);
x = 2;
delta = 0.01;

% Tolerance
tol = 1e-5;

% Iterate
while abs(f(x)) > tol
     x = x - f(x)*delta/(f(x+delta)-f(x));
end
disp(x)