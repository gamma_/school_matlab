
% Bisection method
clear; clc;

% Define a function to find root of
R = 3;
ph2o = 1000;
ps = 200;
g = 9.8;
vs = 4/3*pi()*r.^2;
f=@(h) 100 - pi()*h^2*(3*R - h)/3;

% Define end points on interval
xl=0;
xh=2*r;

% Error tolerence
tol=1e-5;

% Evaluate function at end points
fl=f(xl);
fh=f(xh);

% Check if their is a root within the interval
if sign(fl) == sign(fh)
    error('f(xl) and f(xh) must have different signs')
end

% Plot function
x=linspace(xl,xh,1000);
figure(1); clf(1)
plot(x,f(x))
xlabel('x')
ylabel('f(x)')
set(gca,'Fontsize',20)

% Iterate to find root
xr=1/2*(xl+xh);
while abs(f(xr)) > tol
    
    % Compute midpoint - xr
    % xr=1/2*(xl+xh);
    xr = xl - fl*(xh-xl)/(fh-fl);
    
    % Evaluate function at xr
    fr=f(xr);
    
    % Update interval based on fr
    if sign(fl) ~= sign(fr)
        % Root is in lower subinterval
        % Update the upper bound
        xh=xr;
        fh=fr;
    else
        % Root is in upper subinterval
        % Update the lower bound
        xl=xr;
        fl=fr;
    end
    
    % Plot current root estimate
    hold on
    plot(xr,fr,'o')
    %pause
    
    disp([xl,xr,xh])
end