clear; clc;
% Funcitons
u = @(x,y) (x-4)^2 + (y-4)^2 - 5;
v = @(x,y) x^2 + y^2 - 16;
ux = @(x) 2*(x-4);
uy = @(y) 2*(y-4);
vx = @(x) 2*x;
vy = @(y) 2*y;
% Initial Guess
x = 4;
y = 3;
% Tolerance and Iteration Counter
tol = 1e-6;
iter = 0;
% Loop
while abs(u(x,y)) > tol && abs(v(x,y)) > tol
    iter = iter + 1;
    D = ux(x)*vy(y) - uy(y)*vx(x);
    x = x - (u(x, y)*vy(y)-v(x, y)*uy(y))/D;
    y = y - (v(x, y)*ux(y)-u(x, y)*vx(x))/D;
end
% Output
disp(x)
disp(y)
disp(iter)