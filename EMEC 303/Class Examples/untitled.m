
clear;clc;

%% Setup
L = 1;
K = 0.1;
N = 10;
T = zeros(1,N);
dx = L/(N-1);
dt = 0.01;

%% Boundary Conditions
T(1) = 10;
T(N) = 20;

%% Iterate and Preallocate
n = 150;
Tt = zeros(n,N);
figure(1);clf(1);
for t = 1:n
    Tt(t,:) = T(:);
    for i = 2:N-1
        T(i) = T(i) + dt*K*(T(i-1)-2*T(i)+T(i+1))/dx^2;
    end
    
    plot(linspace(0,L,N),T(:))
    drawnow
end
figure(2);clf(2);
surf(linspace(0,dt*n,n),linspace(0,L,N),Tt')