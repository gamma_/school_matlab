% Solve for Temperature 
% in a 2D flat plate
clear; clc

N=40;    % Number of grid points 
        % in each dimension

x=linspace(0,1,N);
y=linspace(0,1,N);
        
% Boundary conditions
Tbot=0;
Ttop=0;
%Tlef=10;
%Trig=20;

% Preallocate matrices
A=zeros(N^2,N^2);
B=zeros(N^2,1  );

% Interior points
for j=2:N-1
    for i=2:N-1
        n=i+(j-1)*N;  % Row number for this i & j
        A(n,n  )=-4;  % Main diagonal
        A(n,n-1)= 1;  % Left diagonal
        A(n,n+1)= 1;  % Right diagonal
        A(n,n-N)= 1;  % Far left diagonal
        A(n,n+N)= 1;  % Far right diagonal
        B(n,1  )= sin(2*pi*x(i)) + sin(2*pi*y(j));  % RHS of PDE
    end
end
% Left Boundary
i=1;
for j=2:N-1
    n=i+(j-1)*N;    % Row number for this i & j
    A(n,n)=-1;      % Main diagonal
    A(n,n+1)=1;
    %B(n,1)=10;      % Temp on left boundary
end
% Right Boundary
i=N;
for j=2:N-1
    n=i+(j-1)*N;    % Row number for this i & j
    A(n,n)=1;       % Main diagonal
    A(n,n-1) =-1;
    %B(n,1)=20;      % Temp on right boundary
end
% Bottom Boundary
j=1;
for i=1:N
    n=i+(j-1)*N;    % Row number for this i & j
    A(n,n)=1;       % Main diagonal
    B(n,1)=Tbot;      % Temp on bottom boundary
end
% Top Boundary
j=N;
for i=1:N
    n=i+(j-1)*N;    % Row number for this i & j
    A(n,n)=1;       % Main diagonal
    B(n,1)=Ttop;      % Temp on top boundary
end

% Solve for temperature
Tn=A\B;  % A*T=B

% Convert 1D array into 2D matrix
T=zeros(N,N);
for j=1:N
    for i=1:N
        n=i+(j-1)*N;    % Row number for this i & j
        T(i,j)=Tn(n);   % Transfer temp
    end
end

% Plot temperatures
figure(1); clf(1)
surf(x,y,T')
xlabel('x')
ylabel('y')
set(gca,'Fontsize',20)
