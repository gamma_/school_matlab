clear; clc;
f = @(x) x.^3 - 5;
xl = 0;
xh = 10;
fl = f(xl);
fh = f(xh);
tol = 1e-6;

if sign(fl) == sign(fh)
    error('Higher bound and Lower bound have the same sign.')
end

x = linspace(xl, xh, 1000);
figure(1); clf(1);
plot(x, f(x))
xlabel('x');
ylabel('f(x)');
set(gca, 'Fontsize', 16);

while fh-fl > tol
    xm = xl - fl*(xh-xl)/(fh-fl);
    fm = f(xm);
    if fm == 0
        break
    elseif fm > 0
        xh = xm;
        fh = fm;
    elseif fm < 0
        xl = xm;
        fl = fm;
    end
    hold on
    plot(xm, fm, 'o')
    
end
