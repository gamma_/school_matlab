clear; clc;

%% Initial Values
xo = 0;
to = 10;
tf = 50;
C = -5;
L  = 5;

%% Setup Matricies
N = 100;
dx = L/N;
A = zeros(N,N);
b = zeros(N,1);

%% Set Boundary Conditions
A(1,1) = 1;
A(end, end) = 1;
b(1,1) = to;
b(end, end) = tf;

%% Iterate
for row = 2:N-1
    x = row * dx + xo;
    A(row, row-1) = 1;
    A(row, row) = -2;
    A(row, row+1) = 1;
    b(row) = -C*dx^2*(x-2)^2;
end

% Final Matrix Division
T = A\b;

%% Plot
xl = linspace(xo, xo+L, N);
plot(xl, T(:,1), 'r')
set(gca, 'FontSize', 16)
title('Temperature vs. Position')
xlabel('Position on Bar (m)')
ylabel('Temperature (C)')