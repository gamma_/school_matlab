clear;clc;

% Initial Constants
h = .01;
xo = 0;
yo = 2/25;
xf = 3;
n = (xf-xo)/h;
c1 = 2;
q = 0.1;
v = 5;

% Setup Functions
syms x y
dydx = 5*(y-x^2); % Given Differential Equation
y = x^2 + x/2.5 + 2/25; % Particular Solution to Given Diff. Eq.
dydx = matlabFunction(dydx);
y = matlabFunction(y);

% Create Figure & Preallocate
figure(1); clf(1)
posrk = zeros(n,2);
posab = zeros(n,2);
xo = [xo, xo];
yo = [yo, yo];
erro = 0;
% Iterate
for i = 1:n
    % Add values to plot
    posrk(i,1) = xo(1);
    posrk(i,2) = yo(1);
    % RK-4 Method
    k1 = dydx(xo(1), yo(1)); 
    k2 = dydx(xo(1) + h/2, yo(1) + k1*h/2); 
    k3 = dydx(xo(1) + h/2, yo(1) + k2*h/2); 
    k4 = dydx(xo(1) + h, yo(1) + k3*h); 
    yo(1) = yo(1) + h*(k1 + 2*k2 + 2*k3 + k4)/6;
    xo(1) = xo(1) + h;
    % Max Error
    ya = y(xo(1));
    err = abs(ya - yo(1));
    if err > erro
        maxrk = err;
    end
    erro = err;
end
% Heun's for first value
posab(1,1) = xo(2);
posab(1,2) = yo(2);
y1 = yo(2) + h*dydx(xo(2), yo(2));
ave = (dydx(xo(2)+h,y1)+dydx(xo(2), yo(2)))/2;
yo(2) = yo(2) + ave*h;
xo(2) = xo(2) + h;
erro = 0;
% Iterate
for i = 2:n
    % Add Values to plot
    posab(i,1) = xo(2);
    posab(i,2) = yo(2);
    % AB-2 step Method
    yo(2) = yo(2) + 3*h*dydx(xo(2), yo(2))/2 ...
        - h*dydx(posab(i-1,1),posab(i-1,2))/2;
    xo(2) = xo(2) + h;
    % Max Error
    ya = y(xo(2));
    err = abs(ya - yo(2));
    if err > erro
        maxab = err;
    end
    erro = err;
end
% Plot 
plot(posrk(1:i,1),posrk(1:i,2))
hold on
plot(posab(1:i,1),posab(1:i,2))
xl = linspace(0, xf, 1000);
plot(xl,y(xl))
set(gca, 'FontSize', 13)
legend('RK-4 Method', 'AB-2 Step Method', 'Analytical',...
    'Location', 'northwest')
title('Parasitic ODE');xlabel('t');ylabel('y')
% Print Max Errors
fprintf('Max Error RK-4: %1.5e\nMax Error AB-2: %1.5e\n',maxrk,maxab)