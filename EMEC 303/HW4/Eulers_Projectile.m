clear;clc;

% Initial Constants
h = 0.05;
toc = 0;
tob = 0;
tf = 0.5;
vob = 0; % m/s
voc = 0; % m/s
n = tf/h+1;
g = 9.81; % m/s^2
p = 1.225e3; % g/m^3
mb = 52; % g
mc = 1; % g
Cb = 0.82; % Approximate
Cc = 1.1; % Appoximate
Ab = 0.013;
Ac = 0.01;

% Experimental Data
data = xlsread('HW4.xlsx');
t = linspace(0,tf,5);
vb = data(1:5, 4).';
vc = data(8:12, 4).';
clear data

% Setup Functions
dvdt = @ (v,Cd,Ac,m) g-p*v^2*Cd*Ac/(2*m); % Given Differential Equation; Considering drag
v = @(t) g*t; % Particular Solution to Given Diff. Eq.; Not considering drag

% Create Figure & Preallocate
figure(1); clf(1)
tr = linspace(tob, tf, 1000);
posb = zeros(n,2);
posc = zeros(n,2);
% Iterate
for i = 1:n
    posb(i,1) = tob;
    posb(i,2) = vob;
    posc(i,1) = toc;
    posc(i,2) = voc;
    % Euler's Method
    v1b = vob + h*(dvdt(vob, Cb, Ab, mb));
    v1c = voc + h*(dvdt(voc, Cc, Ac, mc));
    tob = tob + h;
    vob = v1b;
    toc = toc + h;
    voc = v1c;
end
% Plot Wrist Brace
subplot(2,1,1)
plot(t, vb)
hold on
plot(tr, v(tr))
plot(posb(1:n,1),posb(1:n,2))
title('Velocity of Wrist Brace')
xlabel('Time(s)')
ylabel('Velocity(m/s)')
legend('Experimental', 'Analytical', 'Euler''s Method','Location', 'southeast')
% Plot Paper Circle
subplot(2,1,2)
plot(t, vc)
hold on
plot(tr, v(tr))
plot(posc(1:n,1),posc(1:n,2))
title('Velocity of Paper Circle')
xlabel('Time(s)')
ylabel('Velocity(m/s)')
% Add Legend
legend('Experimental', 'Analytical', 'Euler''s Method', 'Location', 'southeast')
