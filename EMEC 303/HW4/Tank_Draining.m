clear;clc;

% Initial Constants
s = 1;
xo = 0; %s
ho = 1; % m
dl = 1; % m
do = 0.1; % m
g = 9.81; % m/s^2

% Setup Functions
syms x h
dhdx = -do^2/dl^2*sqrt(2*h*g); % Given Differential Equation
dhdx = matlabFunction(dhdx);

% Create Figure & Preallocate
figure(1); clf(1)
pos = zeros(1,2);
i = 0;
% Iterate
while isreal(ho)
    i = i+1;
    pos(i,1) = xo;
    pos(i,2) = ho;
    % Original Euler Method
    h1 = ho + s*(dhdx(ho));
    % Average slope for Heun's Method
    ave = (dhdx(h1)+dhdx(ho))/2;
    % Improved Guesstimate using Heun
    h1 = ho + s*ave;
    xo = xo + s;
    ho = h1;
end
    plot(pos(1:i,1),pos(1:i,2))
    title('Draining Tank')
    xlabel('Time(s)')
    ylabel('Height of Water in Tank(m)')
    legend('Heun''s Method')
    fprintf('Time to Empty: %5.2f minutes.\n', xo/60)