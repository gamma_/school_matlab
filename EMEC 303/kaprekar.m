clear;clc;

% Initialize
ni = 4352;

% Loop
n = 0;
while ni ~= 6174
    ns = num2str(ni);
    ni = split(ns,'');
    ni = ni(2:5);
    ni = str2double(ni');
    nsb = sort(ni);
    nbs = sort(ni,'descend');
    nsb = 1000*nsb(1) + 100*nsb(2) + 10*nsb(3) + nsb(4);
    nbs = 1000*nbs(1) + 100*nbs(2) + 10*nbs(3) + nbs(4);
    ni = nbs-nsb;
    n = n + 1;
end