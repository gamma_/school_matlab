clear;clc

% In
m = 5;
k = 0.5;
ci = [0, sqrt(4*m*k)-2.5, sqrt(4*m*k)+1, sqrt(4*m*k)];
l = 1;
        
% Set initial condition
y0 = [5,0];
tspan = [0 30];
figure(1); clf(1)

% Define System of Eqn.
for i = 1:length(ci)
    c = ci(i);
    ode = @(x,y) [y(2); -(c*y(2) + k*y(1))/m];
    [x,y] = ode45(ode,tspan,y0);
    % Plot
    hold on
    plot(x, y(:,1))
    drawnow
    xlabel('x (m)'); ylabel('Displacement');
end
legend('Undamped', 'Underdamped', 'Overdamped', 'Critically Damped', ...
    'Location', 'southwest')
title('Spring System'); xlabel('x'); ylabel('y')