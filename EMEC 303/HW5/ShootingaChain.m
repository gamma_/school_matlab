clear; clc

% Input Constants
yo = 0;
yf = 0;
L = 10;
w = 1;
T = 100;

% Function
f = @(x,y) [y(2); w/T*sqrt(1+(y(2))^2)];

% Setup for While Loop
xspan = [0 L];
guess = -100;
n = 0;
p = 1;  % power for step size
step = 10^p;
tol = 1e-9;
yend = yf-2*tol;
yold = yend;
method = 1;
% Loop
while abs(yend-yf) > tol
    % This if statement determines when it should change the step size and 
    % direction. It also allows for a differing yo and yf.
    if (sign(yold-yf) ~= sign(yend-yf) && yend > yf)
        method = -1;
        p = p - 1;
        step = 10^p;
        guess = guess - step*n;
    elseif (sign(yold-yf) ~= sign(yend-yf) && yend < yf)
        method = 1;
        p = p - 1;
        step = 10^p;
        guess = guess + step*n;
    else
        if  method == -1
            guess = guess - step*n;
        elseif method == 1
            guess = guess + step*n;
        end
    end
    yi = [yo, guess];
    [x,y] = ode45(f,xspan,yi);
    yold = yend;
    yend = y(end,1);
    n = n+1;
end
% Plot
figure(1);clf(1)
plot(x,y(:,1))
title('Hanging Chain');xlabel('Length'); ylabel('Sag')
