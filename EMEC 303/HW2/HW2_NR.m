% Newton Raphson Method
clear; clc;

% Define function
r = 3;
f = @(h) pi()*h.^2.*(3*r - h)/3 - 100;
fp = @(h) pi()*h.*(2*r - h);
x = 1;

% Tolerance
tol = 1e-5;

% Iteration Counter
iter = 0;

% Iterate
while abs(f(x)) > tol
    % Add 1 to the iteration counter
    iter = iter + 1;
    % Compute new estimate
    x = x - f(x)/fp(x);
end
% Output
fprintf('Root = %5.5f \nIterations = %d\n', x, iter)