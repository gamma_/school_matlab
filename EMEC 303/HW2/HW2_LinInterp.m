
% Lin Interp method
clear; clc;

% Define function
r = 3;
f=@(h) pi()*h.^2.*(3*r - h)/3 - 100;

% Define domain
xl=0;
xh=2*r;

% Tolerence
tol=1e-5;

% Find range
fl=f(xl);
fh=f(xh);

% Check if their is a root within the interval
if sign(fl) == sign(fh)
    error('f(xl) and f(xh) must have different signs')
end

% Iterate to find root
xr=1/2*(xl+xh);

% Iteration Counter
iter = 0;

while abs(f(xr)) > tol
    % Add 1 to the iteration counter
    iter = iter + 1;
    
    % Find midpoint w/ lin interp
    xr = xl - fl*(xh-xl)/(fh-fl);
    
    % Evaluate function at midpoint
    fr=f(xr);
    
    % Update interval
    if sign(fl) ~= sign(fr)
        xh=xr;
        fh=fr;
    else
        xl=xr;
        fl=fr;
    end
end
% Output
fprintf('Root = %5.5f \nIterations = %d\n', xr, iter)