function [A,B,D] = ABD_Matricies(N,Q,t)
%ABD_MATRICIES Calculates ABD Matricies
%   Uses the total number of laminae along with the corresponding stiffness
%   matrix and thickness. Q is a cell matrix containing the indexed Q_bar
%   matricies for each laminae. t is an array containing the indexed
%   thickness for each laminae. 

% Solve z_bar for each laminae

z_bar = zeros(1,N);
mid = sum(t)/2;
for k = 1:N
    base = sum(t(1:k))-t(k);
    z_bar(k) = base + t(k)/2 - mid;
end

% Preallocate for ABD

A = zeros(3,3);
B = zeros(3,3);
D = zeros(3,3);

% Solve for ABD

for k = 1:N
    Qk = Q{k};
    for i = 1:3
        for j = 1:3
            A(i,j) = A(i,j) + Qk(i,j)*t(k);
            B(i,j) = B(i,j) - Qk(i,j)*t(k)*z_bar(k);
            D(i,j) = D(i,j) + Qk(i,j)*(t(k)*z_bar(k)^2+t(k)^3/12);
        end
    end
end

% Fix rounding error

for i = 1:3
    for j = 1:3
        if A(i,j) < 1
            A(i,j) = 0;
        end
        if B(i,j) < 1
            B(i,j) = 0;
        end
        if D(i,j) < 1
            D(i,j) = 0;
        end
    end
end
% Unit Conversion for input of mm and GPa
A = A.*1000;
B = B.*1000;
D = D.*1000;
