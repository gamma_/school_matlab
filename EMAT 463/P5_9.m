%% Solution for Problem 5.5 in Barbero
% Setup
clear
clc
% Define theta
theta = 45; % in degrees
% Solve for T matrix and inverse
[T, T_inv] = T_Matrix(theta,0);
% Define Q
Q = [35.32, 1.06, 0;
    1.06, 3.53, 0;
    0, 0, 1.75];
% Solve
Q_bar = T_inv*Q*(T_inv')