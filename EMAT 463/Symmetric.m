function [theta] = Symmetric(theta)
%SYMMETRIC Makes a Layup Symmetric
%   If a specified layup details symmetry, this function will intake the
%   array of angles for the layup and append the flipped version to the
%   end.

theta = [theta, fliplr(theta)];

end

