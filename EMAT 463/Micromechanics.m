function [E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm)
%MICROMECHANICS Summary of this function goes here
%   Detailed explanation goes here
Vm = 1-Vf;
E1 = Vf*Ef+Vm*Em;
eta = 2; % Assumed value for circular or square fibers
nu = (((Ef/Em)-1)/((Ef/Em)+eta));
E2 = Em*((1+eta*nu*Vf)/(1-nu*Vf));
Gm = Em/(2*(1+vf));
Gf = Ef/(2*(1+vm));
G12 = Gm*((1+Vf+(1-Vf)*Gm/Gf)/(1-Vf+(1+Vf)*Gm/Gf));
v12 = Vf*vf+vm*Vm;
end

