%% Take Home Portion for Quiz 2 EMAT 463
clear
clc

%% Part 1 [90_2/0_2/+-45_4]s

% Properties E-glass/Epoxy 3501-6
Ef = 72.35;
Em = 5.1;
vf = .22;
vm = .34;

% Given Vf
Vf = 0.54;

% Find laminae properties
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);

% Laminate Properties
theta = [90,90,0,0,45,-45,45,-45,45,-45,45,-45];
[theta] = Symmetric(theta);
N = length(theta);
t1(1:N) = 0.5;
% tl = sum(t);
Q = cell(N);

% Index Q_bar for each Laminae
for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

% Solve for ABD Matricies
[A,B,D] = ABD_Matricies(N,Q,t1);

% Display Solutions
fprintf('Part 1:\n\nA(MPa-mm):\n')
disp(A)
fprintf('B(MPa-mm^2):\n')
disp(B)
fprintf('D(MPa-mm^3):\n')
disp(D)

%% Part 2 [+-45/0_2]

% Properties Kevlar 49/Vynil
Ef = 131;
Em = 3.4;
vf = .35;
vm = .38;

% Given Vf
Vf = 0.6;

% Find laminae properties
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);

% Laminate Properties
theta = [45,-45,0,0];
N = length(theta);
t2(1:N) = 0.5;
% tl = sum(t);
Q = cell(N);

% Index Q_bar for each Laminae
for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

% Solve for ABD Matricies
[A,B,D] = ABD_Matricies(N,Q,t2);

% Display Solutions
fprintf('Part 2:\n\nA(MPa-mm):\n')
disp(A)
fprintf('B(MPa-mm^2):\n')
disp(B)
fprintf('D(MPa-mm^3):\n')
disp(D)

%% Part 3 [0/90/90/0]s

% Properties Hexcel IM7/8552 Epoxy
Ef = 276;
Em = 4.67;
vf = .2;
vm = .37;

% Given Vf
Vf = 0.62;

% Find laminae properties
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);

% Laminate Properties
theta = [0,90,90,0];
[theta] = Symmetric(theta);
N = length(theta);
t3(1:N) = 0.5;
% tl = sum(t);
Q = cell(N);

% Index Q_bar for each Laminae
for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

% Solve for ABD Matricies
[A,B,D] = ABD_Matricies(N,Q,t3);

% Display Solutions
fprintf('Part 3:\n\nA(MPa-mm):\n')
disp(A)
fprintf('B(MPa-mm^2):\n')
disp(B)
fprintf('D(MPa-mm^3):\n')
disp(D)