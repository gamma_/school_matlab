function [Q_bar] = Transform(Q,T_inv)
%TRANSFORM Transforms a Stiffness Matrix
%   Given a stiffness matrix it uses the inverse T matrix to transform Q
%   into Q_bar. 

Q_bar = T_inv*Q*T_inv';

end

