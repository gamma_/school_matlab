clear all
clc

%% Aluminum

E1 = 71;
E2 = 71;
v12 = .3;
G12 = E1/(2*(1+v12));
t = [1];
tl = sum(t);
theta = [0];
N = length(t);
Q = cell(1,N);

for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

[Aa,Ba,Da] = ABD_Matricies(N,Q,t);

%% Carbon/Epoxy [0/90]S


E1 = 142;
E2 = 10.3;
v12 = .27;
G12 = 7.2;
t = [1,1,1,1];
tl = sum(t);
theta = [0,90,90,0];
N = length(t);
Q = cell(1,N);

for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

[Al,Bl,Dl] = ABD_Matricies(N,Q,t);

%% Carbon/Epoxy [+_ 45]2


E1 = 142;
E2 = 10.3;
v12 = .27;
G12 = 7.2;
t = [1,1,1,1];
tl = sum(t);
theta = [45,-45,45,-45];
N = length(t);
Q = cell(1,N);

for k = 1:N
    [T,T_inv] = T_Matrix(theta(k),0);
    [Qk] = Q_Matrix(E1,E2,G12,v12);
    [Qk] = Transform(Qk,T_inv);
    Q{k} = Qk;
end

[An,Bn,Dn] = ABD_Matricies(N,Q,t);