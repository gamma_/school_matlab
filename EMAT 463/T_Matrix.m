function [T,T_inv] = T_Matrix(theta,flag,varargin)
%T_MATRIX Takes a given angle and out puts the respective T-Matrix
%   The function first checks if the theta given is in radians or degrees.
%   It then converts if nessecary. Using the theta both the T-Matrix and
%   its inverse are calculated and output.

% Look I learned something new!
%   varargin allows functions to take in a variable amount of inputs, then
%   you set an if then statement up to determine how many were specified.
%   If there are less than the required amount it will set the unspecified
%   variable to whatever default you want.

if nargin < 2
    flag = true; % In this case I am assuming the input is in radians
end

% If otherwise specified, it checks to see if the flag is set to false or 0.
if flag == false || flag == 0
    theta = theta*pi()/180; % Convert theta to radians
end

% T Matrix
m = cos(theta);
n = sin(theta);
T = [m^2, n^2, 2*m*n;
    n^2, m^2, -2*m*n;
    -m*n, m*n, m^2-n^2];
% Inverse of T Matrix
T_inv = [m^2, n^2, -2*m*n;
    n^2, m^2, 2*m*n;
    m*n, -m*n, m^2-n^2];
end

