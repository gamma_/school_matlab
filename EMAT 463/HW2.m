% Solutions to suggested problems from EMAT 463 Week 2
% clear
clc
%% Calculate T Matrix
theta = 30; % Theta in degrees
[T, T_inv] = T_Matrix(theta,0);

%% Given Stress States
sig_1o = [5;13;10];
sig_2o = [10;-10;0];

%% Find using Tensor
disp('Tensor Rotation')
sig_1 = T*sig_1o
sig_2 = T_inv*sig_2o
return
%% Find using Mohr's Circle
disp("Mohr's Circle")
p = (sig_1o(1)+sig_1o(2))/2;
q = (sig_1o(1)-sig_1o(2))/2;
r = sqrt(q^2+sig_1o(3)^2);
theta_o = acos(sig_1o(1)/r);
theta_f = theta_o - 2*theta;
sig_1 = [abs(r*cos(theta_f))+p;p-abs(r*cos(theta_f));r*sin(theta_f)]
return
p = (sig_2o(1)+sig_2o(2))/2;
q = (sig_2o(1)-sig_2o(2))/2;
r = sqrt(q^2+sig_2o(3)^2);
theta_o = acos(sig_2o(1)/r);
theta_f = theta_o + 2*-theta;
sig_2 = [r*cos(theta_f)+p;p-r*cos(theta_f);r*sin(theta_f)]