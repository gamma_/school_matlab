function [Ex,Ey,Gxy,vxy] = Moduli(A,tl)
%MODULI Calculates Laminate Moduli
%   Uses Eqn. 6.42 from, 'Intro to Composite Materials Design'. Requires a
%   known A matrix and laminate thickness.
Ex = (A(1,1)*A(2,2) - A(1,2)^2)/(tl*A(2,2));
Ey = (A(1,1)*A(2,2) - A(1,2)^2)/(tl*A(1,1));
Gxy = A(3,3)/t;
vxy = A(1,2)/A(2,2);
end

