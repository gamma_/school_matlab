function [Q] = Q_Matrix(E1,E2,G12,v12)
%Q_MATRIX Creates a Stiffness Matrix
%   Intakes laminae properties and uses equations given from 'Introduction
%   to Composite Materials Design' to calcuate a stiffness matrix for the 
%   laminae.
delta = 1 - v12^2*E2/E1;
Q = [E1/delta, v12*E2/delta, 0;
    v12*E2/delta, E2/delta, 0;
    0, 0, G12];
end

