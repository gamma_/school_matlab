%% Take Home Portion for Quiz 1
clear
clc

%% Question 1 Material Selection
% Choose random number between 150 and 155
E1 = 152;
% Preallocate for Data Storage
var = zeros(3,4);
% T300 Material Properties
Ef = 230;
% vf = .2;
% Matrix Properties Matrix ;)
M = [2.8, 3.4, 4.02, 5.1;
    .38, .38, .36, .34];
for i = 1:4
    % Solve for E2 and Vf w/ Material Properties
    [Vf, E2] = find_E2(E1, Ef, M(1,i));
    % Add to Data Storage
    var(1,i) = E2;
    var(2,i) = Vf;
end

% Plot data for comparison
figure(1)
bar(1:4, var(1,:))
ylabel('E2 (GPa)')
set(gca,'xticklabel',{'BPA', 'Vinyl','914-C', '3501-6'})
% Different matricies have a large effect on E2
figure(2)
bar(1:4, var(2,:))
ylabel('Volume Fraction (Vf)')
ylim([.6,.7])
set(gca,'xticklabel',{'BPA', 'Vinyl','914-C', '3501-6'})
% Different matricies have little effect on the Volume Fraction

%% Question 2
% Given
% Material Properties for E-glass/3501-6 Epoxy
Ef = 72.35;
Em = 5.1;
vf = .22;
vm = .34;
% Volume Fraction
Vf = .54;

% Solve
fprintf('Initial Stiffness Matricies\n\n')
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);
[Q2] = Q_Matrix(E1,E2,G12,v12);
% Display Data
fprintf('Q2\n')
disp(Q2);

%% Question 3
% Given
% Material Properties for Kevlar 49/Vynil Ester
Eg = 131;
Ee = 3.4;
vf = .35;
vm = .38;
% Volume Fraction
Vf = .60;

% Solve
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);
[Q3] = Q_Matrix(E1,E2,G12,v12);
% Display Data
fprintf('Q3\n')
disp(Q3);

%% Question 4
% Given
% Material Properties for Hexcel IM7/8552 Epoxy
Ef = 276;
Em = 4.67;
vf = .2;
vm = .37;
% Volume Fraction
Vf = .62;

% Solve
[E1,E2,G12,v12] = Micromechanics(Vf,Ef,Em,vf,vm);
[Q4] = Q_Matrix(E1,E2,G12,v12);
% Display Data
fprintf('Q4\n')
disp(Q4)

%% Question 5
% Define T and T_inv Matricies for 30 degrees
[T, T_inv] = T_Matrix(30,0);

% Solve for QX_bar at 30 degrees
[Q2_bar] = Transform(Q2, T_inv);
[Q3_bar] = Transform(Q3, T_inv);
[Q4_bar] = Transform(Q4, T_inv);

% Display Values
fprintf('Q2-4 Transformed at 30 degrees\n\n')
fprintf('Q2_bar\n')
disp(Q2_bar)
fprintf('Q3_bar\n')
disp(Q3_bar)
fprintf('Q4_bar\n')
disp(Q4_bar)

% Define T and T_inv Matricies for 45 degrees
[T, T_inv] = T_Matrix(45,0);

% Solve for QX_bar at 45 degrees
[Q2_bar] = Transform(Q2, T_inv);
[Q3_bar] = Transform(Q3, T_inv);
[Q4_bar] = Transform(Q4, T_inv);

% Display Data
fprintf('Q2-4 Transformed at 45 degrees\n\n')
fprintf('Q2_bar\n')
disp(Q2_bar)
fprintf('Q3_bar\n')
disp(Q3_bar)
fprintf('Q4_bar\n')
disp(Q4_bar)

function [Vf, E2] = find_E2(E1, Ef, Em)
% Solve for Volume Fraction
Vf = (E1-Em)/(Ef-Em);
% Solve for E2
E2 = ((1-Vf)/Em+Vf/Ef)^(-1);
end