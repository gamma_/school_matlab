clear; clc;

sig = [750 500 0;500 5000 0;0 0 250];

prin_val = sort(eig(sig))

sig_3 = prin_val(3);
sig_1 = prin_val(1);
sig_2 = prin_val(2);

t_max = sig_3 - sig_1

oct = sqrt((sig_1-sig_2)^2+(sig_1-sig_3)^2+(sig_2-sig_3)^2)