clear; clc;

d = .1;
sig = [0 4*42.33/(pi()*d^2)+160/(pi()*d^3) 0; 
    4*42.33/(pi()*d^2)+160/(pi()*d^3) 0 0; 
    0 0 32*10.58333/(pi()*d^2)];
prin = sort(eig(sig));
s1 = prin(3); s2 = prin(2); s3 = prin(1);
eq = s1-s3;
ideal = 63000/2.25;

while abs(eq/ideal-1) > 0.0001
    sig = [0 4*42.33/(pi()*d^2)+160/(pi()*d^3) 0; 
    4*42.33/(pi()*d^2)+160/(pi()*d^3) 0 0; 
    0 0 32*10.58333/(pi()*d^2)];
    prin = sort(eig(sig));
    s1 = prin(3); s2 = prin(2); s3 = prin(1);
    eq = s1-s3;
    if eq > ideal
        d = d + d*0.001;
    elseif eq < ideal
        d = d - d*0.001;
    end
end
d