function [sig_1,sig_2,sig_3] = hookeslaw(strain, E, v)
%STRAINTOSTRESS Converts a strain tensor into principal stresses
%       The strain tensor is put in with units of microns and the
%   corresponding principal strains are found. With these principal strains
%   the principal stresses are calculated, on line 25, using E (Young's 
%   Modulous), input in units of Pa, and v(Poisson's Ratio).

%% Principal Strain Calculation
[E1,E2,E3] = invariant(strain);
prin_E = [E1;E2;E3]*10^-6;

%% Poisson's Ratio Matrix Setup
V = zeros(3,3);
for i = 1:3
    for j = 1:3
        if i == j
            V(i,j) = 1-v;
        else
            V(i,j) = v;
        end
    end
end

%% Final Calculation of Principal Stresses
prin_sig = sort((E/((1+v)*(1-2*v))*V)*prin_E);

sig_1 = prin_sig(3);
sig_2 = prin_sig(2);
sig_3 = prin_sig(1);
end