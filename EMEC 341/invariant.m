function [sig1,sig2,sig3] = invariant(sig)
%INVARIANT.m Finds principal stresses/strains using Invariant method

%% Invariant Calculations
i1 = trace(sig);
i2 = sig(1,1)*sig(2,2)+sig(1,1)*sig(3,3)+sig(2,2)*sig(3,3)-sig(1,2)^2-sig(1,3)^2-sig(2,3)^2;
i3 = det(sig);

%% Root Finding and Solution
prin = sort(roots([1, -i1, i2, -i3]));
sig1 = prin(3);
sig2 = prin(2);
sig3 = prin(1);
end

