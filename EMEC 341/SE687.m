clear; clc;

dP = [.05 .07 .09 .12 .15 .17 .19 .21 .23 .25];
Q = [2 2.35 2.7 3.12 3.5 3.72 3.85 4.1 4.35 4.45];
lndp = log(dP);
lnq = log(Q);
syms x
F = (0.502)*x + 2.2;
F = matlabFunction(F);
sum = 0;
sumln = 0;
for i = 1:length(dP)
    sum = sum + (dP(i) - F(Q(i)))^2;
    sumln = sumln + (lndp(i) - F(lnq(i)))^2;
end
SE = sqrt(sum/(length(dP)-2))
SEln = sqrt(sumln/(length(dP)-2))


