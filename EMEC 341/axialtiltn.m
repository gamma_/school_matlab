clear; clc;
%% Initial Var.

% Stress Matrix
sigma = [-2, .25, 0;
    .25, 2.5, 0;
    0, 0, 0];

% Unit Vector Matrix
 unit_v = [ -.987 .154 .028;
     .0381 .0628 .9973;
     .152 .985 .068];

%% Problem Setup

% Allocate Space for Final Matrix
sigma_p = zeros(3,3);

%% Solve

% For loops
for row = 1:3
    for col = 1:3
        sigma_p(row, col) = (unit_v(row,:)*sigma)*unit_v(col, :)';
    end
end

% Display Solution
disp(sigma_p)
