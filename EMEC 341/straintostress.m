function [sig] = straintostress(strain, E, v)
%STRAINTOSTRESS Converts a strain tensor into a stress tensor
%       The strain tensor is put in with units of microns and the
%   corresponding principal strains are found. With these principal strains
%   the principal stresses are calculated, on line 25, using E (Young's 
%   Modulous), input in units of Pa, and v(Poisson's Ratio).

% Convert Microns to Meters
strain = strain*10^-6;
%% Poisson's Ratio Matrix Setup
V = zeros(3,3);
for i = 1:3
    for j = 1:3
        if i == j
            V(i,j) = 1-v;
        else
            V(i,j) = v;
        end
    end
end

%% Final Calculation of Principal Stresses
norm_E = [strain(1,1);strain(2,2);strain(3,3)];
sig = zeros(3,3);
for i = 1:3
    for j = 1:3
        if i == j
            sig(i,j) = (E*V(i,:)/((1+v)*(1-2*v)))*norm_E;
        else
            sig(i,j) = (E/(1+v))*strain(i,j);
        end
    end
end
end

