clear; clc;
%% Initial Var.

% Theta (radians)
theta = pi()/6;

% Stress Matrix
sigma = [-2, .25, 0;
    .25, 2.5, 0;
    0, 0, 0];

%% Problem Setup

% Unit Vector Calculation (for rotation about z-axis)
 unit_v = [ cos(theta), sin(theta), 0;
     -sin(theta), cos(theta), 0;
     0, 0, 1];

% Allocate Space for Final Matrix
sigma_p = zeros(3,3);

%% Solve

% For loops
for row = 1:3
    for col = 1:3
        sigma_p(row, col) = (unit_v(row,:)*sigma)*unit_v(col, :)';
    end
end

% Display Solution
disp(sigma_p)
