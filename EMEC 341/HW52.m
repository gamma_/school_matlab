clear; clc;

syms r
N = 1000;

sig_rr = 22.32 - 45200.89/r^2;
sig_tt = 22.32 + 45200.89/r^2;
sig_zz = zeros(1,N);
sig_zz(:) = 22.32;
tau_zt = 2.14*r;

sig_rr = matlabFunction(sig_rr);
sig_tt = matlabFunction(sig_tt);
tau_zt = matlabFunction(tau_zt);

r = linspace(25, 45, N);

figure(1)
plot(r, sig_rr(r),'r')
grid on
hold on
plot(r, sig_tt(r),'b')
plot(r, sig_zz,'g')
plot(r, tau_zt(r),'k')
hold off
title('Stresses vs. Radius','fontSize', 20)
ylabel('MPa')
xlabel('mm')
legend('σ_r_r', 'σ_θ_θ', 'σ_z_z', 'τ_z_θ')

figure(2)
sig1 = zeros(1,N);
sig2 = zeros(1,N);
sig3 = zeros(1,N);

for i = 1:N
    %% Sigma if Tau_zt != 0
    sig = [sig_rr(r(i)) 0 0;0 sig_tt(r(i)) tau_zt(r(i));0 tau_zt(r(i)) sig_zz(1)];
    %% Sigma if Tau_zt = 0
    %sig = [sig_rr(r(i)) 0 0;0 sig_tt(r(i)) 0;0 0 sig_zz(1)];
    %% Solve for Principal Stresses
    [sig1(i), sig2(i), sig3(i)] = invariant(sig);
end
plot(r, sig1,'r')
hold on
plot(r, sig2,'b')
plot(r, sig3,'g')
plot(r, sig1-sig3,'k')
hold off
title('Principal Stresses vs. Radius','fontSize', 20)
ylabel('MPa')
xlabel('mm')
legend('σ_1', 'σ_2', 'σ_3', 'τ_m_a_x')