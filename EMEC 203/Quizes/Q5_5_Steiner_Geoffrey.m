%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 11th 2018
clear all
close all

load Q4_Data.mat

criteria = [];
for i = 1:length(TV')
    if TV(i) > 200 || TV(i) < 140 && Sports(i) == 18
        criteria(1,i) = 1;
    else
        criteria(1,i) = 0;
    end
end
minimum = 1000;
for i = 1:length(criteria)
    if criteria(i) == 1
        mini = Relax(i);
        if mini < minimum
            minimum = mini;
        else
            continue
        end
    end
end

fprintf('People in the age range 35-44 spend only %d minutes relaxing.\n', minimum)