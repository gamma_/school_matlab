%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 27th 2018
clear all
close all
load('S5 _MSWdata.mat');
x = [1:6];
plot(MSWdata(1,:),MSWdata(2,:),'g-.s', 'MarkerFaceColor', 'm');
%I have no idea as to how to make a trendline... I don't think we were ever
%taught how to.
x_tick = {'1960', '1970', '1990', '2000', '2010'};
legend('MSWdata');
xticks(MSWdata(1,:));
text(MSWdata(1,1),MSWdata(2,1),'<--matches trend');
text(MSWdata(1,2),MSWdata(2,2),'<--matches trend');
text(MSWdata(1,3),MSWdata(2,3),'<--matches trend');
text(MSWdata(1,4),MSWdata(2,4),'<--matches trend');