%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 9th 2018
clear all
close all

load Q4_Data.mat
age_ranges = {'15-19','20-24','25-34','35-44','45-54','55-64','65-74','75+'};
first_cond = age_ranges(Read>=Relax&Sports>24|Social>=42);
tv_comp = Comp + TV;
second_cond = age_ranges(find(min(tv_comp)));
fprintf('People in the age range %s spend %d minutes watching TV\n', second_cond{1,1}, TV(find(min(tv_comp))));