%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 25th 2018
clear all
close all
% load BozemanPrecipitationData.xls
months = table2array(readtable('BozemanPrecipitationData.xls', 'Range', 'B1:B576'))';
precip = table2array(readtable('BozemanPrecipitationData.xls', 'Range', 'C1:C576'))';
sum_months = zeros(1,12);
% ave_months = zeros(1,12);

for i = 1:12
    tot = 0;
    for x = 1:47
        tot = tot + precip(i * x);
    end
    sum_months(1,i) = tot;
end

Avg = sum_months(1,:) ./ 47;

plot([1:12], Avg);
i = 1;
while i < 13
    x = 1;
    tot = 0;
    while x < 48
        tot = tot + precip(i * x);
        x = x + 1;
    end
    sum_monthsW(1,i) = tot;
    i = i + 1;
end
AvgW = sum_monthsW(1,:) ./ 47;

figure
plot([1:12], AvgW)

if Avg == AvgW
    disp('Correct')
else
    disp('I need more time')
end