% function Q10_5_Steiner_Geoffrey
% %part I
syms z
g = 9.81;
rho = 1000;
D = 60;
z_v = linspace(0,D,D+1);
lint = rho*g*(1.3*z+122)*(D-z);
F_a = vpa(int(lint,z,0,D));
integrand = rho.*g.*(1.3.*z_v+122).*(D-z_v);
force = mylhr(z_v,integrand);
%part II
step = [30,20,10,5,1];
forces = mystep(step,D,integrand);
% end
%Integration with step size
function [int] = mystep(step,final,y)
int = zeros(1,length(step));
for j = 1:length(step)
    x = linspace(0,final,final./step(j));
    for k = 2:length(x)
        int(1,j) = int(1,j) + y(k-1)*step(j);
    end
end
end
%Standard Left hand rule integration
function [int] = mylhr(x,y)
int = 0;
for i = 2:length(x)
    int = int + y(i-1).*(x(i)-x(i-1));
end
end