%Geoffrey Steiner
%Alyssa Cook
%Montana State University
%EMEC-203-005
%September 1st 2018
function Q8_5_Steiner_Geoffrey
clear all
close all
x = random('normal',75,10,[100 1]);
comp_harm = harmmean(x);
myharm = my_harm(x);
disp('Harm Mean')
fprintf('Comp: %.02f Me: %.02f\n', comp_harm, myharm)
comp_geo = geomean(x);
mygeo = geomean(x);
disp('Geomean')
fprintf('Comp: %.02f Me: %.02f\n', comp_geo, mygeo)
end

function [out] = my_geo(in)
out = 1;
for i = 1:length(in)
    out = out * in(i);
end
out = out ^ (1/length(in));
end

function [out] = my_harm(in)
bot = 1 ./ in;
bot = my_sum(bot);
out = length(in) / bot;
end

function [tot] = my_sum(v)
tot = 0;
for i = 1:length(v)
    tot = tot + v(i);
end
end