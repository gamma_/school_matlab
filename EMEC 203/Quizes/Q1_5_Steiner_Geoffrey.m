%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 13th 2018
clear all
close all
%Flushes/day
dailyflushes = input('How many times do you flush the toilet in a day? ');
%Flushes/year
annualflushes = dailyflushes * 365;
%gallons/year
HEannualwater = annualflushes * 1.28;
%gallons/year
STannualwater = annualflushes * 1.6;
%gallons/year
annualwatersaved = STannualwater - HEannualwater;
fprintf('Water saved by using efficient toilet is %.2f\n', annualwatersaved);