% function Q9_5_Steiner_Geoffrey
close all
clear all
load('aerodynamicq9.mat')
comp_next = interp1(h,rho,hstar,'next');
comp_near = interp1(h,rho,hstar,'nearest');
mine_next = my_next(h,rho,hstar);
mine_near = my_near(h,rho,hstar);
subplot(2,1,1)
plot( h, rho,'hk')
hold on
plot( hstar, comp_next,'b+')
plot( hstar, mine_next,'rd')
hold off
title('Next Neighbor Interpolation')
xlabel('Altitude(m)')
ylabel('Density(kg/m^{3})')
legend('Data','Interpolated-MATLAB','Interpolated-Mine')
subplot(2,1,2)
plot( h, rho,'hk')
hold on
plot( hstar, comp_near,'b+')
plot( hstar, mine_near,'rd')
hold off
title('Nearest Neighbor Interpolation')
xlabel('Altitude(m)')
ylabel('Density(kg/m^{3})')
legend('Data','Interpolated-MATLAB','Interpolated-Mine')
% end

function [fstar] = my_next(x,f,hstar)
fstar = zeros(1,length(hstar));
ct = 0;
for i = 1:length(hstar)
    for z = 1:length(x)
        if hstar(i) == x(z)
            fstar(1,i) = f(z);
            break
        elseif hstar(i) > x(z) & hstar(i) < x(z+1)
            fstar(1,i) = f(z+1);
            break
        else
            continue
        end
    end
end
end

function [fstar] = my_near(x,f,hstar)
fstar = zeros(1,length(hstar));
ct = 0;
for i = 1:length(hstar)
    for z = 1:length(x)
        if hstar(i) == x(z)
            fstar(1,i) = f(z);
        elseif hstar(i) > x(z) & hstar(i) < x(z+1)
            if hstar(i)-x(z) < x(z+1)-hstar(i)
                fstar(1,i) = f(z);
            else
                fstar(1,i) = f(z+1);
            end
        else
            continue
        end
    end
end
end