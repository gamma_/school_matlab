%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 9th 2018
clear all
close all
data = [96,67,83,97,146;11,12,13,13,16];
states = {'OR', 'WA', 'ID', 'WY', 'MT'};
foodDesertLand = data(1,:) .* (data(2,:) ./ 100);
aveLandArea = mean(foodDesertLand);
index1 = find(foodDesertLand>aveLandArea);
disp(strcat('States: ', states{index1}))