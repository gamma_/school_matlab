% function Q11_5_Steiner_Geoffrey
g = 9.81;
v = 5;
A = [70,1,0; 60,-1,1;40,0,-1];
b = [(70*g-10*v);(60*g-14*v);(40*g-17*v)];
x_inv = inv(A)*b;
x_div = A\b;
disp('Inverse:')
disp(x_inv)
disp('Left Div:')
disp(x_div)
disp('Gauss-Jordan')
disp(gaussjordan(A,b))
disp('Gauss Elim:')
disp(mygausselim(A,b))
disp('Gauss-Sidel:')
disp(mygaussseid(A,b))
% end

function [x] = gaussjordan(a,b)

sz=size(a);
n=sz(1);
x=zeros(1, n);
a=[a, b];

%eliminates lower diagonal 
for i=1:n-1
    for j=i:n-1
        a(j+1, :)=a(j+1,:)-a(j+1, i)/a(i, i)*a(i, :);
    end
end

%eliminates upper diagonal
for i=(n-1):-1:1
    for j=i:-1:1
        a(j, :)=a(j,:)-a(j, i+1)/a(i+1, i+1)*a(i+1, :);
    end
end

%finish the very last step here
for row = 1:size(a,1)
    a(row,:) = a(row,:)./a(row,row);
end
x = a(:,end);
end

function [solution] = mygausselim(matrix, b)
% This function solves for a system of equations given the cooefficient
% matrix and solution matrix 'b' with the Gaussian Elimination Method.
for col = 1:size(matrix,2)
    for row = col:size(matrix,1)
        if col == row
            b(row) = b(col)./matrix(row,col);
            matrix(row,:) = matrix(row,:)./matrix(row,col);
        elseif row >= col
            b(row) = b(col).*-1.*matrix(row,col)+b(row);
            matrix(row,:) = matrix(col,:).*(-1)*matrix(row,col)+matrix(row,:);
        end
    end
end

for col = size(matrix,2):-1:2
    for row = col-1:-1:1
        b(row) = b(col).*-1.*matrix(row,col)+b(row);
        matrix(row,:) = matrix(col,:).*(-1)*matrix(row,col)+matrix(row,:);
    end
end
solution = b;
end


function [x] = mygaussseid(matrix,b)
% This function solves for a system of equations given the cooefficient
% matrix and solution matrix 'b' with the Gauss-Sidel Iterative Method.
x = zeros(size(matrix,1),1);
for t = 1:100
    for i = 1:length(x)
        bot = matrix(i,i);
        top = matrix(i,:).*x';
        top(i) = 0;
        top = b(i)-sum(top);
        x(i) = top/bot;
    end
end
% I added a check to see if it does/doesn't converge here in the case that
% it dosen't converge or one or more of the answers go to infinity.
cond = sum(isnan(x));
cond1 = sum(isinf(x));
if cond > 0 | cond1 > 0
    x = "Error, doesn't converge";
end
end