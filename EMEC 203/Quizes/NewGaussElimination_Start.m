function [x]=newgauss(a, b)

sz=size(a);
n=sz(1);
x=zeros(1, n);
a=[a, b];

%eliminates lower diagonal 
for i=1:n-1
    for j=i:n-1
        a(j+1, :)=a(j+1,:)-a(j+1, i)/a(i, i)*a(i, :);
    end
end

%eliminates upper diagonal
for i=(n-1):-1:1
    for j=i:-1:1
        a(j, :)=a(j,:)-a(j, i+1)/a(i+1, i+1)*a(i+1, :);
    end
end

%finish the very last step here


end