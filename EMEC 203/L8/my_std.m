function [std_v] = my_std(v)
ave = my_mean(v);
v = v - ave;
v = v.^2;
v_sum = my_sum(v);
std_v = (v_sum / length(v))^(1/2);
end

