function []=L8_5_Steiner_Geoffrey()
close all
load ClimateData_Bozeman.mat
subplot(2,2,1);
scatter(yrs, temp)
title('Ave Temp in Bozeman, MT per year')
xlabel('Years')
ylabel('Ave Temp')
subplot(2,2,2);
scatter(yrs, precip)
title('Ave Precipitation in Bozeman, MT per year')
xlabel('Years')
ylabel('Ave Precip')
subplot(2,2,3);
scatter(yrs, snow)
title('Ave Snowfall in Bozeman, MT per year')
xlabel('Years')
ylabel('Ave Snowfall')
subplot(2,2,4);
scatter(yrs, solarrad)
title('Ave Solar Radiation in Bozeman, MT per year')
xlabel('Years')
ylabel('Ave Solar Radiation')
figure
temp_pre = calc_corrcoef_mine(temp,precip);
temp_sno = calc_corrcoef_mine(temp,snow);
temp_sol = calc_corrcoef_mine(temp,solarrad);
comp_temp_pre = corr2(temp,precip);
comp_temp_sno = corr2(temp,snow);
comp_temp_sol = corr2(temp,solarrad);
fprintf('Mine: %.2f %.2f %.2f \nComp: %.2f %.2f %.2f\n',...
    temp_pre,temp_sno,temp_sol,comp_temp_pre,comp_temp_sno,comp_temp_sol)
subplot(2,2,1)
scatter(temp,precip)
titel = strcat('Ave Precipitation vs. Ave Temperature [1870-2099], Correlation Coefficient = ',num2str(round(temp_pre,2)));
title(titel)
xlabel('Ave Temp')
ylabel('Ave Precip')
subplot(2,2,2)
scatter(temp,snow)
titel = strcat('Ave Snowfall vs. Ave Temperature [1870-2099], Correlation Coefficient = ',num2str(round(temp_sno,2)));
title(titel)
xlabel('Ave Temp')
ylabel('Ave Snowfall')
subplot(2,2,3)
scatter(temp,solarrad)
titel = strcat('Ave Solar Radiation vs. Ave Temperature [1870-2099], Correlation Coefficient = ',num2str(round(temp_sol,2)));
title(titel)
xlabel('Ave Temp')
ylabel('Ave Solar Radiation')
%This last plot below was the only thing I enjoyed out of this completely
%mindless assignment. I only used it because the titles were so long that
%they were overlapping and I didn't want to leave the last subplot empty.
subplot(2,2,4)
imshow 404.png
end
function [corr_coef_xy] = calc_corrcoef_mine(x,y)
std_x = my_std(x);
std_y = my_std(y);
cov_xy = my_cov(x,y);
corr_coef_xy = (cov_xy / (std_x * std_y));
end