function [tot] = my_sum(v)
tot = 0;
for i = 1:length(v)
    tot = tot + v(i);
end
end