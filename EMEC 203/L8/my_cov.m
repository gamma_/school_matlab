function [cov_xy] = my_cov(x,y)
ave_x = my_mean(x);
ave_y = my_mean(y);
x = x - ave_x;
y = y - ave_y;
xy = x .* y;
variable = my_sum(xy);
cov_xy = variable / (length(x) - 1);
end

