function lab11
clc
% Part A: 
% n = 4;
% z = randi(10,[n,1]);
z = [2;1;3];
% test = randi(10,n);
test = [2,2,1;0,1,2;1,1,3];
gausselim = mygausselim(test,z)
% These two match because the matrix works nicely.
gaussseid = mygaussseid(test,z)
% Part B:
A = [-4 -4 7 4 2 3 4 1; 0 -1 0 1 0 0 1 0; 0 0 2 0 0 0 0 0; -1 0 0 1 0 0 0 0; 0 0 0 0 1 0 0 0; 0 0 0 0 0 1 0 0;-1 0 2 0 0 1 2 0; 0 -2 0 0 0 0 0 2];
b2 = [24;0;7;0;42;66;0;96];
x_inv = inv(A)*b2
x_ldiv = A\b2
gausselim2 = mygausselim(A,b2)
% Gaussseid doesn't match because it isn't diagonally dominant.
gaussseid2 = mygaussseid(A,b2)
% Testing my extra function dia_dom()
test = [2,1,1;1,3,2;2,3,5];
for_fun = dia_dom(test)
end

function [solution] = mygausselim(matrix, b)
% This function solves for a system of equations given the cooefficient
% matrix and solution matrix 'b' with the Gaussian Elimination Method.
for col = 1:size(matrix,2)
    for row = col:size(matrix,1)
        if col == row
            b(row) = b(col)./matrix(row,col);
            matrix(row,:) = matrix(row,:)./matrix(row,col);
        elseif row >= col
            b(row) = b(col).*-1.*matrix(row,col)+b(row);
            matrix(row,:) = matrix(col,:).*(-1)*matrix(row,col)+matrix(row,:);
        end
    end
end

for col = size(matrix,2):-1:2
    for row = col-1:-1:1
        b(row) = b(col).*-1.*matrix(row,col)+b(row);
        matrix(row,:) = matrix(col,:).*(-1)*matrix(row,col)+matrix(row,:);
    end
end
solution = b;
end


function [x] = mygaussseid(matrix,b)
% This function solves for a system of equations given the cooefficient
% matrix and solution matrix 'b' with the Gauss-Sidel Iterative Method.
x = zeros(size(matrix,1),1);
for t = 1:100
    for i = 1:length(x)
        bot = matrix(i,i);
        top = matrix(i,:).*x';
        top(i) = 0;
        top = b(i)-sum(top);
        x(i) = top/bot;
    end
end
end

function [cond] = dia_dom(mat)
% This function will take a coefficient matrix and determine whether it is a
% Diagonally Dominant matrix.
cond = zeros(1,size(mat,1));
for i = 1:size(mat,1)
    dia = mat(i,i);
    mat(i,i) = 0;
    tot = sum(mat(i,:));
    if dia >= tot
        cond(1,i) = 1;
    end
end
if sum(cond) == size(mat,1)
    cond = 1;
elseif sum(cond) < size(mat,1)
    cond = 0;
end
end
