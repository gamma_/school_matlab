%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 19th 2018
clear all
close all
while true
    prompt = 'What''s your name? ';
    name = input(prompt,'s');
    fprintf('%s are you ready to calculate some numbers in Matlab?(Y/N)',name);
    cond = input('','s');
    if cond == 'Y'
    else
        disp('Ok nevermind then...')
        break
    end
    prompt = 'Overall Radius: ';
    t = input(prompt);
    prompt = 'Height: ';
    h = input(prompt);
    prompt = 'Radius of Hole: ';
    r = input(prompt);
    sahc = (2*pi*r*h + 2*pi*(r+t)^2 + 2*pi*(r+t)*h)-(2*pi*r^2);
    vhc = (pi*(r+t)^2*h) - (pi*r^2*h);
    fprintf('The surface area of the hollow cylinder is %s in^2\n',num2str(sahc))
    fprintf('The volume of the hollow cylinder is %s in^3\n', num2str(vhc))
    while true
        prompt = 'Which metal do you want to use?(lead/copper/silver/iron) ';
        metal = input(prompt,'s');
        if metal == "lead"
            metal = 0.409;
            break
        elseif metal == "copper"
            metal = .32;
            break
        elseif metal == "silver"
            metal = .38;
            break
        elseif metal == "iron"
            metal = .284;
            break
        else
            continue
        end
    end
    weight_hollow = vhc * metal;
    fprintf('The weight of the hollow cylinder is %s lbs\n', num2str(weight_hollow))
    break
end
