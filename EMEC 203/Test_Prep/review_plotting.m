x = [0:2:100];
y = x.^2 + 2;
subplot(2,1,1)
plot(x,y)%plot
hold on
scatter(x,y)
hold off
subplot(2,1,2)
[X,Y] = meshgrid(x,y);
Z = X.*Y;
surf(X,Y,Z)