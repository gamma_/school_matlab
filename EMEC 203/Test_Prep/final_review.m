string = 'This nasty thing';
vec = [1:.1:10];
revstring(string)
sqrt(4)
4^(1/2)
numel(string)
numel(vec)

function [rev] = revstring(org)
ct = 1;
for i = length(org):-1:1
    rev(ct) = org(i);
    ct = ct + 1;
end
end