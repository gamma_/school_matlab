%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 9th 2018
clear all
close all
angles = [0, 30, 60, 90, 120, 150, 180, 210, 240, 270];
rads = angles * pi / 180;
cos_of_angles = cos(angles);
index_120 = angles(5);
every_other = angles(2:2:8);
x = [1:10];
y = [2:2:100];
s = [1; 2];
trans_x = x';
first_matrix = [1:3; 1:3; 1:3; 1:3;];
M = randi(10, 4, 3);
N = 2 * M + 5;
add_N_M = N + M;
mult_N_M = M .* N;
div_N_M = M ./ N;
square_M = M.^2;
v = ones(3);
b = zeros(4,5);
mat = randi(1000, 1, 100);
M(1 , : ) = 0;
N_resh = reshape(N, 1, 12);
BIG = randi(100,5,6,4);
ex = BIG(1,2,3);
metals = {'lead', 'iron', 'copper'};
density = {0.41, 0.284, 0.324};
l = [density{:}] ./ 2;
fprintf('The density of %s is %s lbs/in^3\n',metals{1},num2str(density{1}));