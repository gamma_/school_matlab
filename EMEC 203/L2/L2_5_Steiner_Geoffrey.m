%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 19th 2018
clear all
close all
% Part A
Type = {'rainfall', 'lakes', 'aquifers', 'resiviors', 'rivers'};
Volume = [119000, 91000, 10000000, 5000, 2120];
gallons = Volume .* 2.642e+11;
fresh_sum = sum(gallons);
global_sum = 200 * fresh_sum;
fprintf('The total amount of avaliable fresh water is %.2e gallons\n', fresh_sum)
fprintf('The total amount of water on earth is %.2e gallons\n', global_sum)
percentages = gallons ./ fresh_sum;
max_vol = max(percentages);
max_ind = find(percentages==max_vol);
max_vol_perc = max_vol * 100;
fprintf('%s%% of all avaliable and usable fresh water comes from %s \n', num2str(max_vol_perc), Type{max_ind})
% Part B
load countries.mat;
load data.mat;
per_capita = data(1:18,3) * 10^9 ./ data(1:18,2);
less_than_1000 = find(per_capita<=1000);
fprintf('Countries with less than 1000 m^3/inhab/year:\n')
for x = less_than_1000
    fprintf('%s \n', countries{x})
end
total_fresh_water_usage = data(19,3);
percentages_v2 = 100 .* data(1:18, 3) ./ total_fresh_water_usage;
greater_than = find(percentages_v2>6.5);
disp('Countries with greater that 6.5% of the global water resources:')
for x = greater_than
    fprintf('%s \n', countries{x})
end