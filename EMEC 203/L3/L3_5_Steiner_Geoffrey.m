%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 13th 2018
clear all
close all
load year_crash_data.mat
load crash_data.mat
crash_data = data;
load data.mat
load selfDrivingCars.mat
tot_miles = sum(crash_data(1,:));
tot_crashes = sum(crash_data(2,:));
subplot(3,2,1:2);
plot(year, crash_data);
title('US Motor Vehicle Statistics');
xlabel('year');
legend('Crashes', 'Miles Driven');
tot_crashes_text = strcat('Total Crashes = ', num2str(tot_crashes));
tot_miles_text = strcat('Total Miles = ', num2str(tot_miles));
text(year(12), (crash_data(1,12)-1000), tot_crashes_text, 'FontSize', 12);
text(year(12), (crash_data(2,12)-1000), tot_miles_text, 'FontSize', 12);
subplot(3,2,3:4);
values = [0.125, 0.5, 1, 10^3, 10^6, 10^9, 10^12, 10^15, 10^18, 10^21];
semilogy([1:10], values, 'b*');
title('Examples of Data Storage');
axis([-inf, inf, 10^-40, 10^40]);
byte = 'byte';
set(gca, 'XTick', [1:10]);
%In order to get the XTick functions to work properly the list has to be a
%Cell Array!
suffixes = {'Bit', 'Nibble', 'Byte', 'Kilobyte', 'Megabyte', 'Gigabyte', 'Terabyte', 'Petabyte', 'Exabyte', 'Zettabyte'};
examples = {'Binary', 'Int', 'Character', 'Text', 'Photo', 'RAM', 'Drive', 'Supercomp', 'WWW', 'A lot'};
text([1:10], values,examples, 'FontSize', 12);
ylabel('Bytes');
set(gca, 'XTickLabel', suffixes);
subplot(3,2,5);
imshow image_selfdrivingcar.jpg;
title('Image from Self-Driving Car');
subplot(3,2,6);
contourf(cars);
colorbar
colormap('cool')