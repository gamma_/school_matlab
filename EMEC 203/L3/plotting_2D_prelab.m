%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 12th 2018
clear all
close all
%Created this for nothing...
%x = zeros(1,40);%preallocation
%for i=[1:40]
%    x(1,i) = (2*pi/40) * i;
%end
x = linspace(0,2*pi,40);
y = cos(2.*x);
z = sin(x);
hold on
plot(x,y);
plot(x,z,'r','LineWidth',2);
hold off
xlabel('x');
ylabel('y');
title('Plot of Cosine + Sine');
axis([pi/2 3*pi/2 -inf inf]);
legend('cos','sin');
%Splitting into two figure windows:
figure
subplot(2,1,1);
plot(x,y);
xlabel('x');
ylabel('y');
title('Plot of Cosine');
subplot(2,1,2);
plot(x,z,'r','LineWidth',2);
xlabel('x');
ylabel('y');
title('Plot of Sine');
axis([pi/2 3*pi/2 -inf inf]);