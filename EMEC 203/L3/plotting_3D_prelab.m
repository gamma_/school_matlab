%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 12th 2018
clear all
close all
rotate3d on
x = [-10:0.5:10];
y = [-10:0.5:10];
[X,Y] = meshgrid(x,y);
Z = sin(sqrt(X.^2 + Y.^2)) ./ sqrt(X.^2 + Y.^2);
surf(X,Y,Z);
camposm(1,1,1);
figure
surfc(X,Y,Z);
colormap winter
figure
contourf(X,Y,Z);
%Whoops.. that actually printed a piece of paper from my printer... Haha
%print
print('-RGBImage')
figure
contourm(X,Y,Z);