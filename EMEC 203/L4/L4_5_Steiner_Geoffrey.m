%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 9th 2018
clear all
close all
trash = {'Glass Bottle', 'Furniture', 'Computer', 'Plastic Bottle', 'Styrofoam', 'Wood', 'Li-Battery', 'Printer Cartrige'};
min_dist = [2.71,4.25,0.92,.03,.79,1.22,4.84,1.16];
max_dist = [84.07,248.54,269.4,283.54,294.96,515.89,3975.58,6115.71];
avg_dist = [18.4,79.46,101.24,27.15,46.33,92.36,1246.15,1713.57];
avg_decomp = [500,30,300,450,5000,15,100,100];
avg_GHG = [1,4,5,1,2,5,66,91];
avg_GHG = avg_GHG(:) ./ 1000;
x = [1:8];
subplot(2,1,1);
%Note: To resize markers use 'markers' followed by a comma and the
%preffered font size.
semilogy(x, max_dist, 'm--.', 'markers', 20);
hold on
semilogy(x, min_dist, 'g--s');
semilogy(x, avg_dist, 'b--o');
hold off
set(gca, 'XTick', x);
set(gca, 'XTickLabels', trash);
title('Distance Trash Travels');
xlabel('Types of Trash');
ylabel('Distances to Travel(km)');
legend('Max Distance', 'Min Distance', 'Avg Distance');
worst = find(max_dist>280&avg_dist>100|avg_decomp>400);
exp = avg_GHG(worst);
max_GHG = max(exp);
subplot(2,1,2);
plot(x, avg_GHG, 'k--o');
title('GHG Emissions for Trash');
ylabel('GHG(MTCE/US Ton)');
xlabel('Types of Trash');
set(gca, 'XTick', x);
set(gca, 'XTickLabels', trash);
text(find(avg_GHG==max_GHG) , max_GHG, '<--- Worst Trash'); 