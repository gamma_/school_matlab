%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 14th 2018
clear all
close all
load raw_data.mat
count = 0;
pl_rad = [];
for i = raw_rad_data
    if i > 0
        count = count + 1;
        pl_rad(1,count) = i;
    else
        continue
    end
end
rad_Earth = 3959;
rad_Neptune = 15299;
rad_Jupiter = 43441;
bt_Jupiter = 0;
lt_Jupiter = 0;
lt_Neptune = 0;
SES = 0;
lt_Earth = 0;
for i = pl_rad
    if i > 1
        bt_Jupiter = bt_Jupiter + 1;
    elseif i > (rad_Neptune/rad_Jupiter)
        lt_Jupiter = lt_Jupiter + 1;
    elseif i > 2 * (rad_Earth/rad_Jupiter)
        lt_Neptune = lt_Neptune + 1;
    elseif i > (rad_Earth/rad_Jupiter)
        SES = SES + 1;
    elseif i < (rad_Earth/rad_Jupiter)
        lt_Earth = lt_Earth + 1;
    end
end
radius_data = [lt_Earth,SES,lt_Neptune,lt_Jupiter,bt_Jupiter];
radius_types = {'smaller than Earth', 'Super Earth Size', 'smaller than Neptune', 'smaller than Jupiter', 'bigger than Jupiter'};
bar([1:5], radius_data)
set(gca, 'XTick', [1:5])
set(gca, 'XTickLabels', radius_types)