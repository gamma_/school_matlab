%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 14th 2018
clear all
close all
load raw_data.mat
comp = 1/11.2;
count = 0;
indicies = [];
raw_dist_sun = (raw_ratdor_data .* raw_st_rad_data) ./ 215;
for i = 1:length(raw_eqt_data)
    if raw_eqt_data(i) > 187 && raw_rad_data(i) > (0.4 * comp) && raw_orb_data(i) > 91 && raw_dist_sun(i) > 0.75 && raw_eqt_data(i) < 294 && raw_rad_data(i) < (2.5 * comp) && raw_orb_data(i) < 801 && raw_dist_sun(i) < 395.6
            count = count + 1;
            indicies(1,count) = i;
    else
        continue
    end
end
disp('These exoplanets are within the habitable zone:')
for i = indicies
    disp(pl_names(1,i))
end
% count = 0;
% for x = indicies
%     count = count + 1;
%     if raw_eqt_data(x) < 294 && raw_rad_data(x) < (2.5 * comp) && raw_orb_data(x) < 801 && raw_dist_sun(x) < 395.6
%         continue
%     else
%         indicies(1,count) = 0;
%     end
% end
% save important_data.mat comp indicies