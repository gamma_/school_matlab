clear all
pl_names = table2cell(readtable('planets.xlsx', 'Range', 'B2:B3303', 'ReadVariableNames', false))';
raw_rad_data = table2array(readtable('planets.xlsx', 'Range', 'AA2:AA3303', 'ReadVariableNames', false))';
raw_orb_data = table2array(readtable('planets.xlsx', 'Range', 'F2:F3303', 'ReadVariableNames', false))';
raw_eqt_data = table2array(readtable('planets.xlsx', 'Range', 'CI2:CI3303', 'ReadVariableNames', false))';
raw_ratdor_data = table2array(readtable('planets.xlsx', 'Range', 'EN2:EN3303', 'ReadVariableNames', false))';
raw_st_rad_data = table2array(readtable('planets.xlsx', 'Range', 'BJ2:BJ3303', 'ReadVariableNames', false))';
save raw_data.mat