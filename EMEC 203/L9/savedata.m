% function savedata
hour = table2array(readtable('SolarDNI_Minute.xlsx', 'Range', 'A1:A893'))';
minute = table2array(readtable('SolarDNI_Minute.xlsx', 'Range', 'B1:B893'))';
DNI = table2array(readtable('SolarDNI_Minute.xlsx', 'Range', 'C1:C893'))';
hour_ave = table2array(readtable('SolarDNI_Hourly.xlsx', 'Range', 'A1:A24'))';
DNI_ave = table2array(readtable('SolarDNI_Hourly.xlsx', 'Range', 'B1:B24'))';
save data.mat
clear all
close all
% end