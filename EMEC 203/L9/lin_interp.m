function [fstar] = lin_interp(x,f,xstar)
%LIN_INTERP This assumes that the x and f input is already sorted. (Because I
%can't use the sort function :( )
fstar = zeros(1,length(xstar));
ct = 0;
for i = 1:length(xstar)
    for z = 1:length(x)
        if xstar(i) == x(z)
            fstar(1,i) = f(z);
        elseif xstar(i) > x(z) & xstar(i) < x(z+1)
            fstar(1,i) = f(z) + (f(z+1)-f(z)) * (xstar(i)-x(z)) / (x(z+1)-x(z));
            break
        else
            continue
        end
    end
end
end

