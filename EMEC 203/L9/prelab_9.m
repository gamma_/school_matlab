%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 25th 2018
clear all
close all
load act_data
% scatter(temp, density)
exp = interp1(temp, density, 63);
exp1 = interp1(temp, density, 188);
exp2 = interp1(temp, density, 188, 'spline');
exp3 = interp1(temp, density, 188, 'nearest');
%Part2
tempi = linspace(100,270);
densityi = interp1(temp, density, tempi);
%scatter(tempi, densityi)
%Part3
load x y b
load y x
% surf(x,y,b)
interp2(x,y,b,3.67,4.4)
xi = linspace(x(1),x(9),30);
yi = linspace(y(1),y(13),40);
[xxi,yyi] = meshgrid(xi,yi);
bi = interp2(x,y,b,xxi,yyi,'cubic');
surf(xi,yi,bi)