%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 9th 2018

clear all
close all
load data.mat
subplot(1,3,1)
hourmintomax = hour_ave(hour_ave>=min(hour)&hour_ave<=max(hour+1));
DNIhourmintomax = DNI_ave(hour_ave>=min(hour)&hour_ave<=max(hour+1));
plot(hourmintomax, DNIhourmintomax)

subplot(1,3,2)
mins = hour.*60+minute;
plot(mins, DNI)

subplot(1,3,3)
ihourmintomax = linspace(min(hourmintomax), max(hourmintomax), length(hour));
iDNIhourmintomax = interp1(hourmintomax, DNIhourmintomax, ihourmintomax);
myDNIhourmintomax = lin_interp(hourmintomax, DNIhourmintomax, ihourmintomax);
scatter(ihourmintomax,iDNIhourmintomax)
hold on
scatter(ihourmintomax,myDNIhourmintomax,'+')%,'MarkerSize', 12)
hold off
legend('computer', 'mine', 'Location', 'northwest')
% Another example of how to add text in a plot
% text(ihourmintomax(iDNIhourmintomax==max(iDNIhourmintomax)), iDNIhourmintomax(iDNIhourmintomax==max(iDNIhourmintomax)), '\rightarrow You cant see the line for the computer, sucess!')

function [fstar] = lin_interp(x,f,xstar)
%LIN_INTERP This assumes that the x and f input is already sorted. (Because I
%can't use the sort function :( )
fstar = zeros(1,length(xstar));
for i = 1:length(xstar)
    for z = 1:length(x)
        if xstar(i) == x(z)
            fstar(1,i) = f(z);
        elseif xstar(i) > x(z) & xstar(i) < x(z+1)
            fstar(1,i) = f(z) + (f(z+1)-f(z)) * (xstar(i)-x(z)) / (x(z+1)-x(z));
            break
        else
            continue
        end
    end
end
end

