%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 26th 2018
clear all
close all
count = 1;
f = zeros(1,10);
while count <= length(f)
    f(1,count) = count^2+2;
    count=count+1;
end
%for x = [1:10]
%    f(1,x) = x^2+2;
%end
x = [-2*pi:.01:2*pi];
colors = {'r','b','k','g','m','c'};
figure
for i = 1:6
    subplot(3,2,i)
    plot(x, sin(i*x), colors{i});
    title(strcat('This is sin(', num2str(i), 'x)')  );
    xlabel('x');
    grid on;
end
z = [-1 3 7 2 4 0];
v = z(z<=2);
