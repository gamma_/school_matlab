%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%September 27th 2018
clear all
close all
T_c = input('Temp of the Cold Res. (In Kelvin)');
if T_c <= 0
    count = 0;
    while T_c <= 0
        count = count+1;
        if count > 2
            T_c = input('Needs to be a positive number in Kelvin ');
        else
            T_c = input('Temp of the Cold Res. (In Kelvin)');
        end
    end
end
T_h = input('Temp of the Hot Res. (In Kelvin)');
if T_h <= 0
    count = 0;
    while T_h <= 0
        count = count+1;
        if count > 2
            T_h = input('Needs to be a positive number in Kelvin ');
        else
            T_h = input('Temp of the Hot Res. (In Kelvin)');
        end
    end
end
if T_c > T_h
    carnot = 1-T_h/T_c;
    disp('Warning: Your cold resivoir was hotter than your reported hot resivoir. These have been swapped to prevent an incorrect value.')
    fprintf('Your Carnot efficiency is %.2f\n.',carnot)
else
    carnot = 1-T_c/T_h;
    fprintf('Your Carnot efficiency is %.2f.\n',carnot)
end