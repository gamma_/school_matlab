% clear all
% close all
% syms x real;
% h = (sin(x))^2;
% ih = int(h,x);
% int(h, x,-pi,pi)
% ezplot(h,[-pi,pi]);
% 
% g = cos(x)*sin(x);
% int(g,x,0,pi/2)

xd = linspace(0,pi/2,20);
fd = sin(xd).*cos(xd);
trapz(fd)

t = [1:10];
r = [-2 -4 -6 -8 -9 -10 -9 -8 -7 -4];
% plot(t,r)
% trapz(r)

p = [0,2,3,6,7,7.3,7.6,9.5,9.4,8.99,8];
diff(p)