syms x real;
f = (x^3+1) / (2*x-3);
df = diff(f);
ddf = diff(df);
ezplot(ddf)
hold on
% ezplot(df)
grid
hold off
subs(f, x, 1)
figure
syms y real;
g = cos(x)*(y^2-1);
gy = diff(g,y);
ezcontour(g);