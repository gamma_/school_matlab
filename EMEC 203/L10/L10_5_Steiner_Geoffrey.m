function L10_5_Steiner_Geoffrey
close all
% This is how to plot with two y-axes. It's unlikely that I will remember
% this but it may come in handy in the future.
subplot(2,1,1)
load tourdeFrance2.mat
yyaxis left
plot(time,speed)
title('Data')
xlabel('Time (hrs)')
ylabel('Speed (km/hr)')
yyaxis right
plot(time,elevationdata)
ylabel('Elevation')
subplot(2,1,2)
comp = trapz(speed, time);
mine = my_trap(speed, time);
fprintf('Length of S16: %d km\n',mine)
comp = diff(elevationdata);
mine = my_diff(elevationdata);
if comp == mine
    disp('Comp and Mine are the same.')
end
time_min = time .* 60;
plot(time_min(1:end-1),mine)
axis([0 max(time_min) -inf inf])
title('Vertical Speed vs. Time')
xlabel('Time (mins)')
ylabel('Speed (m/min)')
end

function [out] = my_diff(in)
% This function finds the difference in y values taking a step value of
% one.
out = zeros(1,length(in)-1);
for i = 2:length(in)
    out(1,i-1) = in(i)-in(i-1);
end
end

function [area] = my_trap(y,x)
% This function finds the area underneath a curve, given by arrays x and y,
% using the trapezoidal rule.
area = 0;
for i = 2:length(y)
%     dx = v(i)-v(i-1);
    part = y(i) + y(i-1);
    area = area + part;
end
area = (x(2)-x(1))*area/2;
end