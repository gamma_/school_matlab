function [] = lab6_b()
data2 = enter_data(2);
[ave, sum] = calc_stats(data2);
fprintf('The individual averages of each test were %.2f and %.2f\nThe calculated weighted sum is %.2f\n',ave(1,1),ave(1,2),sum)
end