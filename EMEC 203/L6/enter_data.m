function [out_array] = enter_data(no_trials)
out_array = cell(no_trials,4);
for row = 1:no_trials
    name = input(strcat('Name of trial no. ', num2str(row), ': '), 's');
    out_array{row, 1} = name;
    for col = 1:3
        data = input(strcat('Data no. ', num2str(col), ': '));
        out_array{row, col + 1} = data;
    end
end
end