%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 4th 2018
function [temp_cv, temp_max, temp_min, temp_avg] = L6_A_5_Steiner_Geoffrey(temp,flag)
%This converts given temperatures from F to C or vice versa. It also pumps
%out the max, min and avg.

if flag == 'f' || flag == 'F'
    temp_cv = (temp - 32).*(5/9);
    temp_max = max(temp_cv);
    temp_min = min(temp_cv);
    temp_avg = sum(temp_cv)/length(temp_cv);
elseif flag == 'c' || flag == 'C'
    temp_cv = (temp .* 9/5) + 32;
    temp_max = max(temp_cv);
    temp_min = min(temp_cv);
    temp_avg = sum(temp_cv)/length(temp_cv);
else
    disp('Invalid secondary value');
    temp_cv = 0;
    temp_max = 0;
    temp_min = 0;
    temp_avg = 0;
end
end
