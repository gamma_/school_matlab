function [averages, weight_sum] = calc_stats(data)
averages = zeros(1,3);
% data = data{1:size(data,1),2:4};
for i = 1:size(data,1)
    stuff = zeros(1,3);
    for x = 1:3
        stuff(1,x) = data{i,x+1};
    end
    averages(1,i) = mean(stuff);
end
weight_sum = .2*averages(1,1) + .2*averages(1,2) + .4*averages(1,3);
end
