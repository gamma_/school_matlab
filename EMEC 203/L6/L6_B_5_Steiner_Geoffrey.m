%Geoffrey Steiner
%Montana State University
%EMEC-203-005
%October 4th 2018
clear all
close all
% arr = enter_data(3);
% [ave, sum] = calc_stats(arr);
lab6_b
function [out_array] = enter_data(no_trials)
out_array = cell(no_trials,4);
for row = 1:no_trials
    name = input(strcat('Name of trial no. ', num2str(row), ': '), 's');
    out_array{row, 1} = name;
    for col = 1:3
        data = input(strcat('Data no. ', num2str(col), ': '));
        out_array{row, col + 1} = data;
    end
end
end

function [averages, weight_sum] = calc_stats(data)
averages = zeros(1,3);
% data = data{1:size(data,1),2:4};
for i = 1:size(data,1)
    stuff = zeros(1,3);
    for x = 1:3
        stuff(1,x) = data{i,x+1};
    end
    averages(1,i) = mean(stuff);
end
weight_sum = .2*averages(1,1) + .2*averages(1,2) + .4*averages(1,3);
end

function [] = lab6_b()
data2 = enter_data(2);
[ave, sum] = calc_stats(data2);
fprintf('The individual averages of each test were %.2f and %.2f\nThe calculated weighted sum is %.2f\n',ave(1,1),ave(1,2),sum)
end